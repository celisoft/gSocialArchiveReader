#!/usr/bin/env python3

import gi

from parsers.diaspora import DiasporaParser
from parsers.twitter import TwitterParser

gi.require_version('Gtk', "3.0")
from gi.repository import Gtk


class MainWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title="gSocialArchiveReader")
        self.set_default_size(1024, 600)

        self.grid = Gtk.Grid()
        self.add(self.grid)

        self.create_toolbar()
        self.create_textview()

    def create_toolbar(self):
        toolbar = Gtk.Toolbar()
        self.grid.attach(toolbar, 0, 0, 3, 1)

        button_open = Gtk.ToolButton()
        button_open.set_icon_name("document-open-symbolic")
        button_open.connect("clicked", self.on_open_clicked)
        toolbar.insert(button_open, 0)

        button_cal = Gtk.ToolButton()
        button_cal.set_icon_name("x-office-calendar-symbolic")
        button_cal.connect("clicked", self.on_cal_clicked)
        toolbar.insert(button_cal, 1)

        button_search = Gtk.ToolButton()
        button_search.set_icon_name("system-search-symbolic")
        button_search.connect("clicked", self.on_search_clicked)
        toolbar.insert(button_search, 2)

    def on_open_clicked(self, widget):
        # Create a FileChooser :
        dialog = Gtk.FileChooserDialog("Choose a file", self,
                                       Gtk.FileChooserAction.OPEN,
                                       (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                                        Gtk.STOCK_OPEN, Gtk.ResponseType.OK))

        self.add_filechoose_filters(dialog)

        response = dialog.run()

        # If the user choose a file
        if response == Gtk.ResponseType.OK:
            self.textbuffer = self.textview.get_buffer()
            content = ''
            parser = None
            archive_path = dialog.get_filename()

            # For the moment, it's just json or csv
            if archive_path.endswith('json'):
                parser = DiasporaParser(archive_path)
            else:
                parser = TwitterParser(archive_path)

            # Parsing
            if parser is not None:
                for post in (parser.get_statuses()):
                    content += str(post)
                self.textbuffer.set_text(content)

        dialog.destroy()

    def add_filechoose_filters(self, dialog):
        """
        Defines the mime filetypes that can be choose through the FileChooser
        :param dialog: the FileChooser dialog
        """
        # Diaspora* exports JSON
        filter_diasp = Gtk.FileFilter()
        filter_diasp.set_name("Diaspora* archive (*.json)")
        filter_diasp.add_mime_type("application/json")
        dialog.add_filter(filter_diasp)

        # Twitter exports CSV
        filter_twitter = Gtk.FileFilter()
        filter_twitter.set_name("Twitter archive (*.csv)")
        filter_twitter.add_mime_type("text/csv")
        dialog.add_filter(filter_twitter)

    def on_cal_clicked(self, widget):
        print("Will be used to make search with date")

    def on_search_clicked(self, widget):
        print("Will be used to search text in post")

    def create_textview(self):
        scrolled_window = Gtk.ScrolledWindow()
        scrolled_window.set_hexpand(True)
        scrolled_window.set_vexpand(True)
        self.grid.attach(scrolled_window, 0, 1, 3, 1)

        self.textview = Gtk.TextView()
        self.textview.set_wrap_mode(Gtk.WrapMode.WORD)

        scrolled_window.add(self.textview)


win = MainWindow()
win.connect("delete-event", Gtk.main_quit)
win.show_all()

Gtk.main()
