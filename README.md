gSocialArchiveReader
==============

What is this ?
------------------------

The goal of this application is to let you read the archive from social networks.
In the future, it is possible that it will be able to resend message from the archive onto other social networks such as Mastodon.

Technologies used
-----------------

The language used to create the code is Python 3.

The frontend is using GTK 3 through PyGObject python library.

What is the license of this diary ?
-----------------------------------

The code is under GPL v3 license.

